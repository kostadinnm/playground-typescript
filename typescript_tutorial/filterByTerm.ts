interface Link {
  description?: string;
  id?: number;
  url: string;
  [index: string]: any; // string | number | undefined;
}

interface TranslatedLink extends Link {
  language: string;
}

type Links = Link[];

function filterByTerm(input: Link[], searchTerm: string, lookupKey: string = 'url') : Links {
  if (!searchTerm) {
    throw Error('searchTerm cannot be empty');
  }
  if (!input.length) {
    throw Error('input cannot be empty');
  }
  const regex = new RegExp(searchTerm, 'i');
  return input.filter(function(arrayElement) {
    return arrayElement[lookupKey].match(regex);
  });
}

const obj1: Link = { url: 'string1' };
const obj2: Link = { url: 'string2' };
const obj3: Link = { url: 'string3' };

const arrayOfLinks: Link[] = [obj1, obj2, obj3];
const term: string = 'java';
filterByTerm(arrayOfLinks, term);

const link1: TranslatedLink = {
  description: 'typescript tutorial for beginners is a tutorial for all the javascript developers...',
  id: 1,
  url: 'www.valentinog.com/typescript',
  language: 'en'
};

filterByTerm(arrayOfLinks, 'string3');