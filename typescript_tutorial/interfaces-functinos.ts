interface Person {
    name: string;
    city: string;
    age: number;
    printDetails(): void; //why this is not caught?!
    // anotherFunc(a: number, b: number): number;
}

const tom: Person = {
    name: 'Tom',
    city: 'Munich',
    age: 33,
    printDetails: function() {
        // console.log(`${this.name} - ${this.city}`);
        return `${this.name} - ${this.city}`
    }
}