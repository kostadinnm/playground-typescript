"use strict";
var _a;
var arr = [
    { code: 'a' },
    { code: 'b' },
    { code: 'c' },
    { name: 'Caty' },
    { name: 'Siri' }
];
var withCode = arr.map(function (elem) {
    if (elem.code) {
        return elem;
    }
});
var notThere = (_a = withCode[3]) === null || _a === void 0 ? void 0 : _a.code;
console.log(notThere);
