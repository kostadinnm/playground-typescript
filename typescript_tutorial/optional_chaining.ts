const arr = [
    { code: 'a' },
    { code: 'b' },
    { code: 'c' },
    { name: 'Caty' },
    { name: 'Siri' }
];

const withCode = arr.map(function(elem) {
    if (elem.code) {
        return elem;
    }
});

const notThere = withCode[3]?.code;
console.log(notThere);