const path = require('path');
const Fiber = require('fibers');

const plugins = {
    html: require('html-webpack-plugin'),
    extractCSS: require('mini-css-extract-plugin')
}

module.exports = {
    entry: {
        app: [
            './app/app.ts',
            './css/app.scss'
        ],
        vendor: [
            './css/vendor.scss'
        ]
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.((s[ac]|c)ss)$/,
                use: [
                    {
                        loader: plugins.extractCSS.loader,
                        options: {
                            publicPath: '../'
                        }
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require('sass'),
                            sourceMap: true,
                            sassOptions: {
                                fiber: Fiber
                            }
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
    },
    devServer: {
        inline: false,
        overlay: {
            warnings: true,
            errors: true
        }
    },
    plugins: [
        new plugins.extractCSS({filename: 'css/[name].css'}),
        new plugins.html({
            template: 'index.html',
            minify: {
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true
            }
        })
    ]
};